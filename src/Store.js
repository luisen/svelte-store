import { writable } from "svelte/store";

export const counter = writable(0);

export const counters = writable({
  counter1: 0,
  counter2: 1,
});
