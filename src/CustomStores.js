import { writable } from "svelte/store";

function counter() {
  const { subscribe, set, update } = writable(0);

  return {
    subscribe,
    increment: () => update((n) => n + 1),
    reset: () => set(0),
  };
}

function counters() {
  const { subscribe, update } = writable({
    count1: 0,
    count2: 1,
  });

  return {
    subscribe,
    increment: () =>
      update((n) => {
        n.count1++;
        n.count2++;
        return n;
      }),
  };
}

export const count = counter();
export const counts = counters();
